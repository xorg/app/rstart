This package includes both the client and server sides implementing
the protocol described in the "A Flexible Remote Execution Protocol
Based on rsh" paper found in the specs/ subdirectory.

This software has been deprecated in favor of the X11 forwarding
provided in common ssh implementations.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The primary development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/app/rstart

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

